<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>
      <?php print $head_title ?>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Author" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body>
  <!--Outer-->
  <div id="outer">
    <!-- Header -->
    <div id="header">
      <?php if ($logo) { ?><div id="logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div><?php } ?>
      <div id="siteName">
        <?php if ($site_name): ?>
          <h1 id='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php endif; ?>
      </div>
      <?php if ($site_slogan): ?><h2><?php print $site_slogan ?></h2><?php endif; ?>
    </div><!--End of Header -->

    <!--Menu -->
    <div id="menu">
      <?php print theme('menu_links', $primary_links) ?> 
    </div><!-- End of Menu -->

    <!--Content-->
    <div id="content">
      <?php print $breadcrumb ?>
        <?php if ($title != ""): ?><h2 class="page-title"><?php print $title ?></h2><?php endif; ?>
          <div class="tabs"><?php print $tabs ?></div>
      <?php print $help ?><br />
      <?php print $messages ?>
      <?php print $content; ?>
    </div><!-- End of Content -->

    <!-- Left Sidebar -->
    <div id="sidebarLeft">
      <?php if ($sidebar_left != ""): ?><?php print $sidebar_left ?><?php endif; ?> 
    </div><!-- End of Left Sidebar -->

    <div id="footer"><!-- Footer -->
      <p><?php print $footer_message; ?></p>
    </div><!-- End of footer -->
  </div><!-- End of outer -->
  <?php print $closure ?>
</body>
</html>
